<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('guest');

Route::get('/register','Auth\RegisterController@showRegistrationForm');
Route::get('/login','Auth\LoginController@showLoginForm')->middleware('guest')->name('login');
Route::post('/login','AuthController@authenticateUser')->middleware('guest');


Route::post('/register','AuthController@register');

Route::get('home', 'HobbyController@index');
Route::get('/logout','Auth\LoginController@logout')->middleware('auth');

Route::post('/hobby','HobbyController@store')->middleware('auth');
Route::get('/hobbies','HobbyController@getAll')->middleware('auth');
