
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('welcome-component', require('./components/WelcomeComponent.vue'));
Vue.component('home-component', require('./components/HomeComponent.vue'));
Vue.component('register-component', require('./components/RegisterComponent.vue'));
Vue.component('login-component', require('./components/LoginComponent.vue'));

//misc
Vue.component('app-loading',		require('./misc/AppLoader.vue'));
Vue.component('btn-loading',		require('./misc/ButtonLoader.vue'));

const app = new Vue({
    el: '#app'
});
