@component('mail::message')
# Hello {{$data['username']}}

You added **{{$data['hobby']}}** to your list of hobbies ninja!

@component('mail::button', ['url' => 'http://samuel-hobbies.herokuapp.com/home'])
View My Hobbies
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
