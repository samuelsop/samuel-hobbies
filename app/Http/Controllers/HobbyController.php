<?php

namespace App\Http\Controllers;

use App\Hobby;
use App\Mail\NewHobbyCreated;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use therealsmat\Ebulksms\EbulkSMS;

class HobbyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EbulkSMS $sms)
    {
        $hobby = [
            'name' => $request->name,
            'user_id' => Auth::id(),
        ];

        $user = Auth::user();

        if (Hobby::create($hobby)) {
            $data = [
                'hobby' => $request->name,
                'username'=> $user->name,
            ];
            try {
                Mail::to( Auth::user()->email)->send(new NewHobbyCreated($data));

                $message = 'Hello '.$user->name.', you just added '.$request->name.' to your hobbies ninja!';
                $recipients = $user->phone_number;
                return $sms->composeMessage($message)
                    ->addRecipients($recipients)
                    ->send();
            }catch(Exception $e){
                return apiFailure($e->getMessage(),[],1);
            }
        }
        return apiSuccess('User Registered Successfully', []);
    }

    public function getAll()
    {
        $hobbies = Auth::user()->hobbies;

        return $hobbies;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hobby  $hobby
     * @return \Illuminate\Http\Response
     */
    public function show(Hobby $hobby)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hobby  $hobby
     * @return \Illuminate\Http\Response
     */
    public function edit(Hobby $hobby)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hobby  $hobby
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hobby $hobby)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hobby  $hobby
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hobby $hobby)
    {
        //
    }
}
