<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Mockery\Exception;

class AuthController extends Controller
{
    public function authenticateUser(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->rememberMe))
        {
            return apiSuccess('User Correct', []);
        }

        return apiFailure('Username and password combination incorrect', [], 1);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return apiFailure($validator->errors(),[],1);
        }

        try {
            event(new Registered($user = $this->create($request->all())));

            $this->guard()->login($user);

            return apiSuccess('User Registered Successfully', []);
        }catch(Exception $e){
            return apiFailure($e->getMessage(),[],1);
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone_number' => 'required|string|min:11'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone_number' => $data['phone_number'],
            'password' => Hash::make($data['password']),
        ]);
    }

    protected function guard()
    {
        return Auth::guard();
    }
}
