<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hobby extends Model
{
    protected $fillable = [
        'name', 'user_id'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class);
    }
}
